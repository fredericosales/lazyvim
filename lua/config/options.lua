-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options hereo

local opt = vim.opt
local stash = vim.g

-- options
opt.autowrite = true
opt.number = true
opt.relativenumber = true
opt.splitright = true
opt.wrap = false

-- vim
vim.autoformat = true
stash.loaded_perl_provider = 0
stash.markdown_recommended_style = 0
stash.python3_host_prog = "/usr/bin/python3"
